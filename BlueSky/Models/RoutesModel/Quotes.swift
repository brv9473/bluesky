//
//  FlyDates.swift
//  BlueSky
//
//  Created by Rostyslav Bodnar on 08.10.2020.
//

import Foundation

struct Quotes: Codable {
    let quotes: [Quote]
    
    enum CodingKeys: String, CodingKey {
        case quotes = "Quotes"
    }
}
