//
//  FlyDateModel.swift
//  BlueSky
//
//  Created by Rostyslav Bodnar on 08.10.2020.
//

import Foundation

struct Quote: Codable {
    var minPrice: Int?
    var quoteDateTime: String?
    
    enum CodingKeys: String, CodingKey {
        
       case minPrice = "MinPrice"
       case quoteDateTime = "QuoteDateTime"
    }
}
