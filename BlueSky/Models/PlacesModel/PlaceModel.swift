//
//  ListPlaceModel.swift
//  BlueSky
//
//  Created by Rostyslav Bodnar on 01.10.2020.
//

import Foundation

struct PlaceModel: Codable {
    var placeId: String?
    var placeName: String?
    var countryId: String?
    var regionId: String?
    var cityId: String?
    var countryName: String?
    
    enum CodingKeys: String, CodingKey {
        case placeId = "PlaceId"
        case placeName = "PlaceName"
        case countryId = "CountryId"
        case regionId = "RegionId"
        case cityId = "CityId"
        case countryName = "CountryName"
        
    }
}
