//
//  ListPlaces.swift
//  BlueSky
//
//  Created by Rostyslav Bodnar on 02.10.2020.
//

import Foundation

struct ListPlaces: Codable {
    let places: [PlaceModel]
    
    enum CodingKeys: String, CodingKey {
        case places = "Places"
    }
}
