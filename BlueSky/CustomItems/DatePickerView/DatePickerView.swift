//
//  DatePickerView.swift
//  BlueSky
//
//  Created by Rostyslav Bodnar on 07.10.2020.
//

import UIKit

protocol DateLabelDelegate {
    func changeDateForLabel(date: String)
    func changeDataLabelWithPicker(date: String)
}

class DatePickerView: UIView {
    
    var picker = UIDatePicker()
    var toolBar = UIView()
    private var cancelBtn = UIButton()
    private var saveBtn = UIButton()
    
    private var cnstrBottom: NSLayoutConstraint?
    private var cnstrTop: NSLayoutConstraint?
    private var selectedDate: String?
    var delegate: DateLabelDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    func setup() {
        setupUI()
        setupCnstr()
    }
    
    private func setupCnstr() {
        NSLayoutConstraint.activate([
            cancelBtn.leadingAnchor.constraint(equalTo: toolBar.leadingAnchor, constant: 10),
            cancelBtn.topAnchor.constraint(equalTo: toolBar.topAnchor, constant:  10),
            cancelBtn.bottomAnchor.constraint(equalTo: toolBar.bottomAnchor, constant: -10),
            
            saveBtn.rightAnchor.constraint(equalTo: toolBar.rightAnchor, constant: -10),
            saveBtn.topAnchor.constraint(equalTo: toolBar.topAnchor, constant: 10),
            saveBtn.bottomAnchor.constraint(equalTo: toolBar.bottomAnchor, constant: -10),
            
            toolBar.topAnchor.constraint(equalTo: self.topAnchor),
            toolBar.leftAnchor.constraint(equalTo: self.leftAnchor),
            toolBar.widthAnchor.constraint(equalTo: self.widthAnchor),
            toolBar.heightAnchor.constraint(equalToConstant: 35),
            
            picker.leftAnchor.constraint(equalTo: self.leftAnchor),
            picker.rightAnchor.constraint(equalTo: self.rightAnchor),
            picker.topAnchor.constraint(equalTo: toolBar.bottomAnchor),
            picker.bottomAnchor.constraint(equalTo: self.bottomAnchor),
        ])
    }
    
    private func setupUI() {
        //        picker.addTarget(self, action: #selector(pickerChanged), for: .valueChanged)
        
        cancelBtn.setTitle("Відмінити", for: .normal)
        cancelBtn.addTarget(self, action: #selector(close), for: .touchUpInside)
        
        saveBtn.setTitle("Добавити", for: .normal)
        saveBtn.addTarget(self, action: #selector(save), for: .touchUpInside)
        
        toolBar.backgroundColor = .lightGray
        toolBar.translatesAutoresizingMaskIntoConstraints = false
        toolBar.layer.shadowColor = UIColor.black.cgColor
        toolBar.layer.shadowOpacity = 1
        toolBar.layer.shadowOffset = .zero
        toolBar.layer.shadowRadius = 3
        addSubview(toolBar)
        
        backgroundColor = .white
        translatesAutoresizingMaskIntoConstraints = false
        
        picker.datePickerMode = .date
        picker.translatesAutoresizingMaskIntoConstraints = false
        addSubview(picker)
        
        cancelBtn.backgroundColor = .lightGray
        cancelBtn.translatesAutoresizingMaskIntoConstraints = false
        toolBar.addSubview(cancelBtn)
        
        saveBtn.backgroundColor = .lightGray
        saveBtn.translatesAutoresizingMaskIntoConstraints = false
        toolBar.addSubview(saveBtn)
    }
    
    func show(on view: UIView) {
        toolBar.alpha = 0
        
        view.addSubview(self)
        translatesAutoresizingMaskIntoConstraints = false
        
        cnstrTop = self.topAnchor.constraint(equalTo: view.bottomAnchor)
        
        
        NSLayoutConstraint.activate([
            cnstrTop!,
            leftAnchor.constraint(equalTo: view.leftAnchor),
            rightAnchor.constraint(equalTo: view.rightAnchor),
        ])
        
        view.layoutIfNeeded()
        
        cnstrTop!.isActive = false
        
        cnstrBottom = self.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        cnstrBottom?.isActive = true
        
        UIView.animate(withDuration: 0.5) {
            view.layoutIfNeeded()
            self.toolBar.alpha = 1
        }
    }
    
    @objc func close() {
        
        if superview == nil {
            return
        }
        
        cnstrBottom?.isActive = false
        cnstrTop!.isActive = true
        
        UIView.animate(withDuration: 0.5, animations: {
            self.superview?.layoutIfNeeded()
            
        }) { (_) in
            self.removeFromSuperview()
        }
    }
    
    @objc func save() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM yyyy"
        selectedDate = dateFormatter.string(from: picker.date)
        delegate?.changeDateForLabel(date: selectedDate ?? "0")
        
        close()
        
    }
}
