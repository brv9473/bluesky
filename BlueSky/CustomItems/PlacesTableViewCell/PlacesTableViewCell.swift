//
//  PlacesTableViewCell.swift
//  BlueSky
//
//  Created by Rostyslav Bodnar on 01.10.2020.
//

import UIKit

class PlacesTableViewCell: UITableViewCell {
    @IBOutlet weak var timeInfoLabel: UILabel!
    @IBOutlet weak var flyTimeLabel: UILabel!
    @IBOutlet weak var priceInfoLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
