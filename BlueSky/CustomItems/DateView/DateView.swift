//
//  DateView.swift
//  BlueSky
//
//  Created by Rostyslav Bodnar on 07.10.2020.
//

import UIKit

class DateView: UIView {
    @IBOutlet var backView: UIView!
    @IBOutlet weak var controlView: UIControl!
    @IBOutlet weak var fromToLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var iconButton: UIButton!
    
    let datePicker = UIDatePicker()
    let myDatePicker = DatePickerView()
    let infoController = InfoViewController()
    var date = Date()
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setup()
    }
    
    private func setup() {
        
        guard let view = Bundle.main.loadNibNamed(String(describing: self.classForCoder), owner: self, options: nil)?.first as? UIView else { return }
        
        addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            view.leftAnchor.constraint(equalTo: self.leftAnchor),
            view.rightAnchor.constraint(equalTo: self.rightAnchor),
            view.heightAnchor.constraint(equalTo: self.heightAnchor),
            view.widthAnchor.constraint(equalTo: self.widthAnchor)
        ])
        
        if #available(iOS 14, *) {
            addSubview(datePicker)
            sendSubviewToBack(datePicker)
            
            datePicker.translatesAutoresizingMaskIntoConstraints = false
            datePicker.minimumDate = datePicker.date
            datePicker.addTarget(self, action: #selector(valueChanged), for: .valueChanged)
            
            backView.isUserInteractionEnabled = false
        } else {
            
            addSubview(myDatePicker)
            sendSubviewToBack(myDatePicker)
            
            myDatePicker.translatesAutoresizingMaskIntoConstraints = false
            myDatePicker.picker.minimumDate = myDatePicker.picker.date
            myDatePicker.picker.addTarget(self, action: #selector(oldPickerValueChanged), for: .valueChanged)
        }
        
        configControlView()
    }
    
    private func configControlView() {
        let date = Date()
        let formatter = DateFormatter()

        controlView.backgroundColor = .white
        controlView.layer.cornerRadius = 16
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = .zero
        layer.shadowRadius = 2
        
        clipsToBounds = true
        
        formatter.dateFormat = "dd.MM.yyyy"
        let result = formatter.string(from: date)
        dateLabel.text = result
        
        if #available(iOS   14, *) {
            NSLayoutConstraint.activate([
                datePicker.leftAnchor.constraint(equalTo: leftAnchor),
                datePicker.rightAnchor.constraint(equalTo: rightAnchor),
                datePicker.topAnchor.constraint(equalTo: topAnchor),
                datePicker.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])
            
        } else {
            NSLayoutConstraint.activate([
                myDatePicker.leftAnchor.constraint(equalTo: leftAnchor),
                myDatePicker.rightAnchor.constraint(equalTo: rightAnchor),
                myDatePicker.topAnchor.constraint(equalTo: topAnchor),
                myDatePicker.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])
        }
 
    }
    
    @objc func valueChanged() {
        let dateFormatter = DateFormatter()
        date = datePicker.date
        dateFormatter.dateFormat = "dd.MM.yyyy"
        let selectedDate = dateFormatter.string(from: date)
        dateLabel.text = selectedDate
    }

    @objc func oldPickerValueChanged() {
        let dateFormatter = DateFormatter()
        date = myDatePicker.picker.date
        dateFormatter.dateFormat = "dd.MM.yyyy"
        let selectedDate = dateFormatter.string(from: date)
        dateLabel.text = selectedDate
    }

}
