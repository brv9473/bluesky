//
//  TableViewExtansion.swift
//  BlueSky
//
//  Created by Rostyslav Bodnar on 01.10.2020.
//

import Foundation
import UIKit

extension UITableView {
    
    func setEmptyView (message: String) {
        let emptyView = UIView(frame: CGRect(x: self.center.x,
                                            y: self.center.y,
                                            width: self.bounds.size.width,
                                            height: self.bounds.size.height))
        let messageLabel = UILabel()
        
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.textColor = .lightGray
        messageLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 18)
        messageLabel.text = message
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        
        emptyView.addSubview(messageLabel)
        
        NSLayoutConstraint.activate([
            messageLabel.centerYAnchor.constraint(equalTo: emptyView.centerYAnchor),
            messageLabel.centerXAnchor.constraint(equalTo: emptyView.centerXAnchor)
        ])
        
        self.backgroundView = emptyView
        self.separatorStyle = .none
    }
    
    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}

