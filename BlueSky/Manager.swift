//
//  Manager.swift
//  BlueSky
//
//  Created by Rostyslav Bodnar on 02.10.2020.
//

import Foundation
import Alamofire

class Manager {
    
    let languageStr = Locale.current.languageCode
    let headers: HTTPHeaders = [
        "x-rapidapi-host": "skyscanner-skyscanner-flight-search-v1.p.rapidapi.com",
        "x-rapidapi-key": "e2554afd51msh87964b8ce385d18p1a51d3jsn8911e682f69b"
    ]
    
    func makingRequest<T: Decodable>(params: [String: String], completion: ((T) -> Void)?) {
        let url = Constans.Url.baseUrl
        let param = params.compactMap { (key, value) -> String in
            return "\(key)\(value)"
        }.joined(separator: "&")
        
        AF.request("\(url)" + "\(param)",
                   headers: headers).responseJSON { response in
                    do {
                        guard let data = response.data else { return }
                        let someResult = try JSONDecoder().decode(T.self, from: data)
                        print(someResult)
                        print("______")
                        completion?(someResult)
                        
                    } catch {
                        print("Decoding error: \(error)")
                    }
                   }
    }
    
    func getCountry(text: String, completion: ((ListPlaces) -> Void)?) {
        let dict = ["/autosuggest/v1.0/UK/GBP/en-" + "\(languageStr?.uppercased() ?? "GB")" + "/?query=": text]
        
        makingRequest(params: dict, completion: completion)
    }
    
    func getDates(text: String, completion: ((Quotes) -> Void)?) {
        let dict = ["/browseroutes/v1.0/US/USD/en-" + "\(languageStr?.uppercased() ?? "GB")" + "/": text]
        
        makingRequest(params: dict, completion: completion)
    }
}
