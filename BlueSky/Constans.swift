//
//  Constans.swift
//  BlueSky
//
//  Created by Rostyslav Bodnar on 06.10.2020.
//

import Foundation

final class Constans {
    
    struct Url {
        static let baseUrl = "https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices"
    }
}
