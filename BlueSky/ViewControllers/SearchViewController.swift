//
//  SearchViewController.swift
//  BlueSky
//
//  Created by Rostyslav Bodnar on 01.10.2020.
//

import UIKit
import Alamofire

class SearchViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    let manager = Manager()
    var list: ListPlaces?
    let debouncer = Debouncer(timeInterval: 2)
    
    var completion: ((PlaceModel?) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        view.endEditing(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if searchBar.canBecomeFirstResponder {
            searchBar.becomeFirstResponder()
        }
    }
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if list?.places.count == 0 {
            tableView.setEmptyView(message: "Введіть що шукаєте")
            
        } else {
            tableView.restore()
        }
        return list?.places.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath)
        
        cell.textLabel?.text = list?.places[indexPath.row].placeName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        completion?(list?.places[indexPath.row])
        
        view.endEditing(true)
        tableView.deselectRow(at: indexPath, animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
}
 
extension SearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            self.debouncer.renewInterval()
            
            self.debouncer.handler = {
                if searchBar.text!.count > 1 {
                    self.manager.getCountry(text: searchBar.text!) { [weak self] (list) in
                        self?.list = list
                        self?.tableView.reloadData()
                    }
                    
                } else {
                    self.tableView.reloadData()
                }
            }
    }
}
