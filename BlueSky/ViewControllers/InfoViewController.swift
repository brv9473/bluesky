//
//  SearchViewController.swift
//  BlueSky
//
//  Created by Rostyslav Bodnar on 01.10.2020.
//

import UIKit
import Alamofire

class InfoViewController: UIViewController {
    
    @IBOutlet weak var fromTextField: UITextField!
    @IBOutlet weak var toTextField: UITextField!
    @IBOutlet weak var fromDateView: DateView!
    @IBOutlet weak var toDateView: DateView!
    @IBOutlet weak var findButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    let manager = Manager()
    var quote: Quotes?
    var date = Date()
    let debouncer = Debouncer(timeInterval: 2)
    var tapOnFromDateView = UITapGestureRecognizer()
    var tapOnToDateView = UITapGestureRecognizer()
    
    var placeIdFrom: String?
    var placeIdTo: String?
    var dateFrom: String?
    var dateTo: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fromTextField.delegate = self
        toTextField.delegate = self

        tableView.delegate = self
        tableView.dataSource = self
        
        let resultCell = UINib(nibName: "PlacesTableViewCell", bundle: nil)
        tableView.register(resultCell, forCellReuseIdentifier: "PlacesTableViewCell")
        
        findButton.addTarget(self, action: #selector(findFly), for: .touchUpInside)
        
        if #available(iOS   14, *) {
            print("iOS 14")
            
        } else {
            tapOnFromDateView = UITapGestureRecognizer(target: self, action: #selector(tapFromDate))
            tapOnToDateView = UITapGestureRecognizer(target: self, action: #selector(tapToDate))
        }

        
        fromDateView.addGestureRecognizer(tapOnFromDateView)
        toDateView.addGestureRecognizer(tapOnToDateView)

        setupTextField()
        setupDateViews()
    }
    
    private func setupDateViews() {
        fromDateView.fromToLabel.text = "Від"
        fromDateView.layer.cornerRadius = 6
        
        toDateView.fromToLabel.text = "До"
        toDateView.layer.cornerRadius = 6
    }
    
    private func setupTextField() {
        fromTextField.placeholder = "Звідки"
        toTextField.placeholder = "Куди"
    }
    
    private func fromatDateFrom() {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        date = fromDateView.datePicker.date
        dateFrom = formatter.string(from: date)
    }
    
    private func formatDateTo() {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        date = toDateView.datePicker.date
        dateTo = formatter.string(from: date)
    }
    
    @objc func findFly() {
        fromatDateFrom()
        formatDateTo()
        
        self.debouncer.renewInterval()
        
        self.debouncer.handler = { [weak self] in
            
            self?.manager.getDates(text: "\(self?.placeIdFrom ?? "0")/" +
                                    "\(self?.placeIdTo ?? "0")/" +
                                    "\(self?.dateFrom ?? "0")" +
                                    "?inboundpartialdate=" +
                                    "\(self?.dateTo ?? "0")")
            { [weak self] (quote) in
                self?.quote = quote
                self?.tableView.reloadData()
            }
        }
    }
    
    @objc func tapFromDate() {
        fromDateView.myDatePicker.show(on: self.view)
    }
    
    @objc func tapToDate() {
        toDateView.myDatePicker.show(on: self.view)
    }
}

extension InfoViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        
        vc.completion = { [weak self] in
            if textField == self?.fromTextField {
                self?.fromTextField.text = $0?.placeName
                self?.placeIdFrom = $0?.placeId
                
            } else {
                self?.toTextField.text = $0?.placeName
                self?.placeIdTo = $0?.placeId
            }
        }
        
        self.present(vc, animated: true, completion: nil)
    }
}

extension InfoViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if quote?.quotes.count == 0 {
            tableView.setEmptyView(message: "Немає дат вильоту")
        } else {
            tableView.restore()
        }
        
        return quote?.quotes.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlacesTableViewCell", for: indexPath) as! PlacesTableViewCell
        
        cell.flyTimeLabel.text = quote?.quotes[indexPath.row].quoteDateTime
        cell.priceLabel.text = "\(quote?.quotes[indexPath.row].minPrice ?? 0)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
}
